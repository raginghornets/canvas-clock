# Canvas Clock

## Objective

Make a clock using HTML5 canvas.

## Motivation

Time is important, so I want to have a clock website.

## Credits

https://www.w3schools.com/graphics/canvas_clock.asp