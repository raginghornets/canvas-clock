let canvasClock = {
  'main': function (
    target = document.body
  ) {
    let canvas = this.createCanvas();
    let ctx = canvas.getContext('2d');
    let center = (canvas) => {
      return {
        'x': canvas.width / 2,
        'y': canvas.height / 2,
      }
    };
    target.appendChild(canvas);

    let radius = center(canvas).x * 0.90;
    let x = center(canvas).x;
    let y = center(canvas).y;
    
    // Initial draw
    canvasClock.drawClock(ctx, radius, x, y, new Date());

    // Loop draw
    setInterval( () => {
        canvasClock.drawClock(ctx, radius, x, y, new Date());
      }, 1000
    );
  },
  'createCanvas': function (
    element = 'canvas',
    width = 600,
    height = 600,
    backgroundColor = 'rgb(51, 48, 47)'
  ) {
    let canvas = document.createElement(element);
    canvas.width = width;
    canvas.height = height;
    canvas.style.backgroundColor = backgroundColor;
    return canvas;
  },
  'drawClock': function (ctx, radius, x, y, date) {
    this.setPosition(ctx, x, y);
    this.drawFace(ctx, radius, x, y);
    this.drawTime(ctx, date, radius);
    this.drawCircle(ctx, radius * 0.1, 'rgb(65, 62, 60)');
  },
  'drawFace': function (ctx, radius) {
    this.drawCircle(ctx, radius);
    this.drawRim(ctx, radius);
    this.drawNumbers(ctx, radius);
  },
  'drawCircle': function (ctx, 
    radius = 200, 
    color = 'rgb(78, 72, 69)'
  ) {
    ctx.beginPath();
    ctx.arc(0, 0, radius, 0, 2 * Math.PI);
    ctx.fillStyle = color;
    ctx.fill();
  },
  'drawRim': function (ctx, radius, 
    color1 = 'rgb(47, 45, 44)',
    color2 = 'rgb(82, 79, 76)',
    color3 = 'rgb(47, 45, 44)'
  ) {
    let grad = ctx.createRadialGradient(0, 0, radius * 0.95, 0, 0, radius * 1.05);
    grad.addColorStop(0, color1);
    grad.addColorStop(0.5, color2);
    grad.addColorStop(1, color3);
    ctx.strokeStyle = grad;
    ctx.lineWidth = radius * 0.1;
    ctx.stroke();
  },
  'drawNumbers': function (ctx, radius, 
    color = 'rgb(252, 247, 228)',
    font = 'Verdana',
    offset = 0.85
  ) {
    ctx.font = radius * 0.15 + 'px ' + font;
    ctx.textBaseline = 'middle';
    ctx.textAlign = 'center';
    for (let number = 1; number < 13; number++) {
      this.drawNumber(ctx, number, color, radius, offset);
    }
  },
  'drawNumber': function (ctx, number, color = 'black', radius, offset = 0.85) {
    let angle = number * Math.PI / 6;
    ctx.fillStyle = color;
    ctx.rotate(angle);
    ctx.translate(0, -radius * offset);
    ctx.rotate(-angle);
    ctx.fillText(number.toString(), 0, 0);
    ctx.rotate(angle);
    ctx.translate(0, radius * offset);
    ctx.rotate(-angle);
  },
  'drawTime': function (ctx, date, radius) {
    this.drawSecondHand(ctx, date, radius);
    this.drawMinuteHand(ctx, date, radius);
    this.drawHourHand(ctx, date, radius);
  },
  'drawHourHand': function (ctx, date, radius, color = 'rgb(252, 247, 228)') {
    ctx.strokeStyle = color;
    let hourHandAngle = date.getHours();
    hourHandAngle = hourHandAngle % 12;
    hourHandAngle = (hourHandAngle * Math.PI / 6) + (date.getMinutes() * Math.PI / (6 * 60)) + (date.getSeconds() * Math.PI / (360 * 60));
    this.drawHand(ctx, hourHandAngle, radius * 0.5, radius * 0.05);
  },
  'drawMinuteHand': function (ctx, date, radius, color = 'rgb(252, 247, 228)') {
    ctx.strokeStyle = color;
    let minuteHandAngle = date.getMinutes();
    minuteHandAngle = (minuteHandAngle * Math.PI / 30) + (date.getSeconds() * Math.PI / (30 * 60));
    this.drawHand(ctx, minuteHandAngle, radius * 0.8, radius * 0.05);
  },
  'drawSecondHand': function (ctx, date, radius, color = 'rgb(203, 58, 29)') {
    ctx.strokeStyle = color;
    let secondHandAngle = date.getSeconds();    
    secondHandAngle = (secondHandAngle * Math.PI / 30);
    this.drawHand(ctx, secondHandAngle, radius * 0.9, radius * 0.02);
  },
  'drawHand': function (ctx, angle = 0, length = 1, width = 1) {
    ctx.beginPath();
    ctx.lineWidth = width;
    ctx.moveTo(0, 0);
    ctx.rotate(angle);
    ctx.lineTo(0, -length);
    ctx.stroke();
    ctx.rotate(-angle);
  },
  'setPosition': function (ctx, x = 0, y = 0) {
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.translate(x, y);
  }
}